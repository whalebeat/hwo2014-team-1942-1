import json
import socket
import sys


class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.training = {}
        self.limitAngleThrottle = 1.0
        self.color = ''
        self.pieces = []
        self.raceSession = {}
        self.cars = []
        self.isQualifying = True

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_init(self, data):
        print("Init")
        self.pieces = data['pieces']
        self.cars = data['cars']
        self.raceSession = data['raceSession']
        try:
             duration = self.raceSession['durationMs']
        except KeyError, e:
            print("It's not qualifying duration!")
            self.isQualifying = False
            pass
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        throttleAmount = 0.5
        for racer in data:
            if data['id']['color'] = self.color:
                carAngle = data['angle']
                pieceIndex = data['piecePosition']['pieceIndex']
                pieceAngle = 0
                try:
                    pieceAngle = self.pieces[pieceIndex]['angle']
                except KeyError, e:
                    print("Straight piece")
                    pass
                if pieceAngle = 0:
                    throttleAmount = 1.0
                else:
                    angleRatio = abs(pieceAngle) / 90
                    if angleRatio >= 1.0:
                        throttleAmount = 0.5
                    else:
                        throttleAmount = 1.0 - angleRatio * 0.5
        self.throttle(throttleAmount)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_car_info(self, data):
        print("Receive car info")
        self.color = data['color']
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_car_info,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
